package com.test.ktorserver.repository

import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.atomic.AtomicInteger

data class Subscription(val expirationDate: Date, val userId: Int) {
    companion object { var lastId = AtomicInteger(0) }
    val id = Subscription.Companion.lastId.getAndIncrement()
}

val userSubscriptions: MutableList<Subscription> = Collections.synchronizedList(mutableListOf(

))


class SubscriptionRepository : Repository<Subscription> {
    override fun exists(id: Int): Boolean {
        return userSubscriptions.find { it1 -> it1.id == id } != null
    }

    override fun findAll(): Iterable<Subscription> {
        return userSubscriptions
    }

    override fun findById(id: Int): Subscription {
        return userSubscriptions.find { it1 -> it1.id == id }!!
    }

    override fun save(t: Subscription): Subscription {
        if(userSubscriptions.any { it1 -> it1.id == t.id })
            throw IllegalArgumentException("User with same id already exists")
        userSubscriptions += t
        return t
    }

    override fun deleteById(id: Int) {
        userSubscriptions.removeIf { it1 -> it1.id == id }
    }

    override fun count(): Int {
        return userSubscriptions.size
    }

    fun findAllForUser(userId: Int): Iterable<Subscription> {
        return userSubscriptions.filter { it1 -> it1.userId == userId }
    }

}