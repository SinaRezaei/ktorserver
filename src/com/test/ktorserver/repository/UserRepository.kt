package com.test.ktorserver.repository

import java.util.*
import java.util.concurrent.atomic.AtomicInteger

data class User(val name: String, val phoneNumber: String, var verified: Boolean = false){
    companion object { var lastId = AtomicInteger(0) }
    val id = lastId.getAndIncrement()
}

val appUsers: MutableList<User> = Collections.synchronizedList(mutableListOf(
    User("Abbas", "+98912*******"),
    User("Mohsen", "+98939*******")
))

class UserRepository : Repository<User> {
    override fun exists(id: Int): Boolean {
        return appUsers.find { it1 -> it1.id == id } != null
    }

    override fun findAll(): Iterable<User> {
        return appUsers
    }

    override fun findById(id: Int): User? {
        return appUsers.find { it1 -> it1.id == id }
    }

    override fun save(t: User): User {
        appUsers += t
        return t
    }

    override fun deleteById(id: Int) {
        appUsers.removeIf { it1 -> it1.id == id }
    }

    override fun count(): Int {
        return appUsers.size
    }

    fun findByPhoneNumber(phoneNumber: String): User? {
        return appUsers.find { it1 -> it1.phoneNumber == phoneNumber }
    }
}



