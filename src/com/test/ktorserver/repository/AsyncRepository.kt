package com.test.ktorserver.repository

interface AsyncRepository<T> {
    suspend fun findAll() : Iterable<T>
    suspend fun findById(id: Int) : T
    suspend fun exists(id: Int) : Boolean
    suspend fun save(t: T) : T
    suspend fun deleteById(id: Int)
    suspend fun count() : Int
}



