package com.test.ktorserver.repository

interface Repository<T> {
    fun findAll() : Iterable<T>
    fun findById(id: Int) : T?
    fun exists(id: Int) : Boolean
    fun save(t: T) : T
    fun deleteById(id: Int)
    fun count() : Int
}



