package com.test.ktorserver

import com.auth0.jwk.JwkProviderBuilder
import freemarker.cache.ClassTemplateLoader
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.*
import io.ktor.auth.jwt.jwt
import io.ktor.features.ContentNegotiation
import io.ktor.freemarker.FreeMarker
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.gson.gson
import io.ktor.html.respondHtml
import io.ktor.http.HttpStatusCode
import io.ktor.http.cio.websocket.DefaultWebSocketSession
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.readText
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.websocket.WebSockets
import io.ktor.websocket.webSocket
import kotlinx.html.*
import com.test.ktorserver.repository.Subscription
import com.test.ktorserver.repository.SubscriptionRepository
import com.test.ktorserver.repository.User
import com.test.ktorserver.repository.UserRepository
import java.util.*
import java.util.concurrent.TimeUnit


fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

data class IndexData(val items: List<Int>)

data class PostUser(val name: String, val phoneNumber: String)
data class Response(val message: String)
data class PostVerification(val phoneNumber: String, val code: String)
data class PostSubscription(val expirationDate: Date, val userId: Int)

val userRepository = UserRepository()
val subscriptionRepository = SubscriptionRepository()


fun Application.module() {
    install(ContentNegotiation) {
        gson()
    }
    install(Authentication) {
        basic(name = "admin") {
            realm = "adminRealm"
            validate { if (it.name == "admin" && it.password == "1234") AdminPrincipal("admin") else null }
        }
        jwt(name="user") {
            verifier(JwkProviderBuilder("https://mydomain/")
                .cached(10, 24, TimeUnit.HOURS)
                .rateLimited(10, 1, TimeUnit.MINUTES)
                .build(), "https://mydomain/")
            realm = "userRealm"
            validate { credentials ->

                val userId = credentials.payload.getClaim("id").asInt()!!
                userRepository.findById(userId)?.let { UserPrincipal(it.id, it.name) }
            }
        }
    }
    install(WebSockets)
    install(FreeMarker) {
        templateLoader = ClassTemplateLoader(Application::class.java.classLoader, "templates")
    }

    val wsConnections = Collections.synchronizedSet(LinkedHashSet<DefaultWebSocketSession>())

    routing {
        route("users") {
            authenticate("admin") {
                get("/") {
                    call.respondHtml {
                        body {
                            h1 { +"Users:" }
                            ul {
                                for (n in userRepository.findAll()) {
                                    li { +"$n" }
                                }
                            }
                        }
                    }
                }
            }
            authenticate("user") {
                post("/register") {
                    val postUser = call.receive<PostUser>()
                    userRepository.save(User(postUser.name, postUser.phoneNumber))
                    val smsSent = SmsService.sendSms(postUser.phoneNumber)
                    if (smsSent)
                        call.respond(Response("SMS sent to " + postUser.phoneNumber))
                    else
                        call.respond(Response("Error sending SMS to " + postUser.phoneNumber))
                }
                post("/verify") {
                    val postData = call.receive<PostVerification>()
                    val user = userRepository.findByPhoneNumber(postData.phoneNumber)
                    user?.also {
                        val codeVerified = SmsService.verifyCode(postData.phoneNumber, postData.code)
                        if (!codeVerified)
                            call.respond(HttpStatusCode.NotFound, Response("Invalid verification code"))
                        else {
                            it.verified = true
                            userRepository.save(it)
                            call.respond(Response("User verified"))
                        }
                    } ?: call.respond(HttpStatusCode.NotFound, Response("No User found with given phone number"))
                }
            }
        }
        route("subscriptions") {
            authenticate("admin") {
                route("subscriptions") {
                    get("/") {
                        call.respond(subscriptionRepository.findAll())
                    }
                    get("/{id}") {
                        val id = call.parameters["id"]!!.toInt()
                        call.respond(subscriptionRepository.findById(id))
                    }
                    post("/{id}") {
                        val id = call.parameters["id"]!!.toInt()
                        val postSubscription = call.receive<PostSubscription>()
                        subscriptionRepository.save(Subscription(postSubscription.expirationDate, postSubscription.userId))
                        call.respond(mapOf("success" to true))
                    }
                    delete("/{id}") {
                        val id = call.parameters["id"]!!.toInt()
                        subscriptionRepository.deleteById(id)
                        call.respond(mapOf("success" to true))
                    }
                }
            }
            authenticate("user") {
                route("user-subscriptions") {
                    get("/") {
                        val user = call.authentication.principal as UserPrincipal
                        call.respond(subscriptionRepository.findAllForUser(user.id))
                    }
                    post("/subscribe") {
                        val user = call.authentication.principal as UserPrincipal
                        if(subscriptionRepository.findAllForUser(user.id).count() > 0){
                            throw IllegalArgumentException("Subscription is already active for current user")
                        }
                        val expirationTime = Calendar.getInstance()
                        expirationTime.add(Calendar.MONTH, 1)
                        subscriptionRepository.save(Subscription(expirationTime.time, user.id))
                        call.respond(mapOf("success" to true))
                    }
                }
            }
        }
        get("/chat") {
            call.respond(FreeMarkerContent("chat.ftl", mapOf("user" to 1), "e"))
//            call.respond(FreeMarkerContent("index.ftl", mapOf("data" to IndexData(listOf(1, 2, 3))), ""))
        }
        webSocket("/chat") {
            wsConnections += this
            try {
                while (true) {
                    val frame = incoming.receive()
                    when (frame) {
                        is Frame.Text -> {
                            val text = frame.readText()
                            // Iterate over all the connections
                            for (conn in wsConnections) {
                                conn.outgoing.send(Frame.Text(text))
                            }
                        }
                    }
                }
            } finally {
                wsConnections -= this
            }
        }
    }
}

data class AdminPrincipal(val name: String) : Principal

data class UserPrincipal(val id: Int, val name: String) : Principal